from django.contrib import admin
from first.models import Userinfo, Social, face, myurls

class SocialAdmin(admin.ModelAdmin):
    list_display=['id','userid','iconname','url']
    search_fields=['iconname']
# Register your models here.
admin.site.register(Userinfo)
admin.site.register(face)
admin.site.register(myurls)
admin.site.register(Social,SocialAdmin)