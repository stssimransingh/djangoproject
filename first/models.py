from django.db import models
from django.contrib.auth.models import User
# Create your models here.
class Userinfo(models.Model):
    user=models.OneToOneField(User,on_delete=models.CASCADE)
    pre_title = models.CharField(max_length = 10)
    dob = models.DateField()
    contact = models.CharField(max_length = 10)
    designation = models.CharField(max_length = 100)
    address = models.TextField()
    image = models.FileField(upload_to='profiles')
    datetime = models.DateTimeField(auto_now_add = True)
class Social(models.Model):
    userid = models.ForeignKey(User,on_delete=models.CASCADE)
    iconname = models.CharField(max_length = 100)
    url = models.CharField(max_length = 100)
    show = models.BooleanField(default = True)

class face(models.Model):
    name=models.CharField(max_length=250)
    image=models.ImageField(upload_to="faces")
    datetime = models.DateTimeField(auto_now_add = True)
class myurls(models.Model):
    user=models.ForeignKey(User,on_delete=models.CASCADE)
    urlfield = models.CharField(max_length=250)
    