from django.shortcuts import render
from django.contrib.auth.models import User
from first.models import Userinfo, Social, face, myurls
from django.contrib.auth import authenticate, login, logout
from django.http import HttpResponseRedirect,HttpResponse
from django.contrib.auth.decorators import login_required
import face_recognition
import os
from PIL import Image, ImageDraw
# Create your views here.
def index(request):
    if request.method == 'POST':
        pre_title = request.POST['pre_title']
        first_name = request.POST['first_name']
        last_name = request.POST['last_name']
        dob = request.POST['dob']
        email = request.POST['email']
        contact = request.POST['contact']
        designation = request.POST['designation']
        username = request.POST['username']
        password = request.POST['password']
        address = request.POST['address']
        check = User.objects.filter(username = username)
        if len(check) > 0:
            return render(request,'index.html', {'status': 'Uset Exist'})
        else:
            data = User.objects.create_user(username, email, password)
            data.first_name = first_name
            data.last_name = last_name
            data.save()
            info = Userinfo(pre_title = pre_title,dob = dob, contact = contact, designation = designation, address = address, user=data)
            info.save()
            if "userimage" in request.FILES:
                image = request.FILES['userimage']
                info.image = image
                info.save()
            return render(request,'index.html', {'status': 'Success'})
    return render(request,'index.html')
def loginuser(request):
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        a = authenticate(username=username, password=password)
        if a:
            if a.is_active:
                login(request, a)
                return HttpResponseRedirect('/facedetect/')
        else: 
            return render(request, 'login.html', {'status': 'Invalid username or password'})
    return render(request, 'login.html')    
@login_required
def dashboard(request):
    return render(request, 'dashboard.html')
@login_required
def social1(request):
    if request.method == 'POST':
        iconname = request.POST['iconname']
        url = request.POST['url']
        user = User.objects.get(id = request.user.id)
        data = Social(userid=user, iconname=iconname, url=url)
        data.save()
    userdata = Social.objects.filter(userid__id = request.user.id)
    return render(request,'social.html')
# def logout1(request):
#     logout(request)
@login_required
def facedetect(request):
    BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
    user = Userinfo.objects.get(user__id=request.user.id)
    img1 = BASE_DIR + '/media/' + str(user.image)
    
    getface = face.objects.all()
    known_face_encoding = []

    known_face_name = []
    for i in getface:
        known_face_name.append(i.name)
        img = BASE_DIR + '/media/' + str(i.image)
        immg = face_recognition.load_image_file(img)
        face_encoding = face_recognition.face_encodings(immg)[0]
        
        known_face_encoding.append(face_encoding)
    # print(known_face_encoding)
    # print(known_face_name)
    test_image = face_recognition.load_image_file(img1)
    # bill_face_encoding = face_recognition.face_encodings(billimg)[0]
    # test_image = face_recognition.load_image_file('./group/group.jpg')

    #find face in image
    face_locations = face_recognition.face_locations(test_image)
    face_encodings = face_recognition.face_encodings(test_image, face_locations)

    #Convert to pil format
    pil_image = Image.fromarray(test_image)

    #Create Image
    draw = ImageDraw.Draw(pil_image)


    for (top, right, bottom, left), face_encoding in zip(face_locations, face_encodings):
        matches = face_recognition.compare_faces(known_face_encoding, face_encoding)
        name = "Unknown Person"
        #Once Match 
        if True in matches:
            first_match_index = matches.index(True)
            name = known_face_name[first_match_index]
        #Draw Box
        draw.rectangle(((left, top), (right, bottom)), outline=(0,0,0))
        text_width, text_height = draw.textsize(name)
        # draw.rectangle(((left, right - text_height - 10),(right, bottom)), fill=(0,0,0), outline=(0,0,0))
        draw.text((left + 6, bottom - text_height - 5), name , fill=(255,255,255))
    del draw
    # pil_image.show()
    img2 = BASE_DIR + '/media/'
    pil_image.save(img2 + str(top)+'.jpg')
    epath = img2 + str(top)+'.jpg'
    usr = User.objects.get(id=request.user.id)
    myurls(user=usr,urlfield=epath).save()
    # return HttpResponse('WORK')
    printableurl = '/media/' +str(top)+'.jpg'
    return render(request,'dashboard1.html', {'img':printableurl})